package airportSecurityState.driver;
import java.lang.Integer;
import airportSecurityState.util.Results;
import airportSecurityState.util.FileProcessor;
import airportSecurityState.util.MyLogger;
import airportSecurityState.util.StringProccessor;
import airportSecurityState.airportStates.*;
public class Driver 
{
     
	public static void main(String[] args) 
	{
		int debugLev;
			try{
				  if(!(args[1].toLowerCase().contains(".txt") && args[0].toLowerCase().contains(".txt"))){
					System.out.println("Please submit valid file locations before running. This error occurs if a .txt file extension isn't found.");
					return;
				   }
			   }
			catch(Exception e){
				System.out.println("First and second parameters should be file locations for input and output");
				return;
			}
			try{
				debugLev = Integer.parseInt(args[2]);
				if(!((debugLev <= 4) && (debugLev >= 0))){
					System.out.println("Invalid Logger Level. Exiting run.");
					return;
				 }
			}
			catch(Exception e){
				System.out.println("Third parameter should be a number to determine debugLevel");
				return;
			}

		FileProcessor inp = new FileProcessor(args[0]);
		Results out = new Results(args[1]);
		MyLogger logging = new MyLogger(debugLev);
		StringProccessor proc = new StringProccessor();
		
		proc.Processing(inp, logging, out);
		 
		
	}
}