package airportSecurityState.airportStates;
import airportSecurityState.airportStates.LOW_RISK;
import airportSecurityState.airportStates.MODERATE_RISK;
import airportSecurityState.airportStates.HIGH_RISK;
import airportSecurityState.util.MyLogger;
public abstract class STATES{
    
    public STATES tightenOrLoosenSecurity(int days, int prohibited, int passengers, MyLogger logging){
        double averageItem = (double)prohibited/days;
        double averagePass = (double)passengers/days;
        
        logging.writeMessage((averageItem+ ", " + averagePass + ", " + days),1);
        
        if(averageItem < 1 && averagePass<4){
          if(!(this instanceof LOW_RISK)){
                logging.writeMessage("Changing state to LOW_RISK", 3);
                logging.writeMessage("Constructed LOW_RISK state",4);
          }
            return (new LOW_RISK());
        }
        if((averageItem>= 1 && averageItem < 2 && averagePass<8) || (averagePass>=4 && averagePass<8 && averageItem<2)){
            if(!(this instanceof MODERATE_RISK)){
                logging.writeMessage("Changing state to MODERATE_RISK", 3);
                logging.writeMessage("Constructed MODERATE_RISK state",4);
            }
            return (new MODERATE_RISK());
        }
            if(!(this instanceof HIGH_RISK)){
                logging.writeMessage("Changing state to HIGH_RISK", 3);
                logging.writeMessage("Constructed HIGH_RISK state",4);
            }
        return (new HIGH_RISK());
    }
    public void processString(String inp){
        ;
    }
    public abstract String operationCommit();
}
