package airportSecurityState.util;
import java.lang.Integer;
import airportSecurityState.util.Results;
import airportSecurityState.util.FileProcessor;
import airportSecurityState.util.MyLogger;
import airportSecurityState.airportStates.*;


public class StringProccessor{
    
    	
    	public void Processing(FileProcessor inp, MyLogger logging, Results out){
    	STATES security = new LOW_RISK();
		logging.writeMessage("Constructed LOW_RISK state", 4);
		
		int numDay = 1;
		int prohItems = 0;
		int passengers = 0;
		
		
		String takeIn;
		int startidx;
		int endidx;
		STATES oldState;
		String prohib[] = {"NailCutter", "Gun", "Blade", "Knife"};
		
		while((takeIn=inp.readLine())!=null){
					logging.writeMessage("Parsing inputLine", 2);
			passengers++;
			for(int i=0;i<prohib.length;i++){
				if(takeIn.contains(prohib[i]))
					prohItems++;
			}
			startidx = takeIn.indexOf(":");
			endidx = takeIn.indexOf(";");
			if(Integer.parseInt(takeIn.substring(startidx+1,endidx)) > numDay)
			  numDay = (Integer.parseInt(takeIn.substring(startidx+1,endidx)));
		 security = security.tightenOrLoosenSecurity(numDay, prohItems, passengers, logging);
		 out.writeLine(security.operationCommit());
		 }
    	}
		 
}