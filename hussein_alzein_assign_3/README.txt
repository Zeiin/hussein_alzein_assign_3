
Assuming you are in the directory containing this README:

## To clean:
ant -buildfile src/build.xml clean
Note that after clearing, I couldn't use the run command you provided directly.
Moving into the src folder and running "ant" on the command line compiles it properly to run.
-----------------------------------------------------------------------
## To compile: 
ant -buildfile src/build.xml all

This way for some reason doesn't work in my IDE, so if the build fails in the way
you've provided, try what I suggest here.
After a clean, or upon opening for the first time please move into my src folder and run "ant"


-----------------------------------------------------------------------
## To run by specifying arguments from command line 
## We will use this to run your code
ant -buildfile src/build.xml run -Darg0="input.txt" -Darg1="output.txt" -Darg2="0" 

Please note that in the instructions given to us, along with the example run code, I couldn't
get this ant command to work. BUT if you run the above command where -Darg0=input.txt is the argument
then my program reads in input.txt inside of hussein_alzein_assign_3/airportSecurityState/input.txt
like the specifications said

-----------------------------------------------------------------------

## To create tarball for submission
ant -buildfile src/build.xml tarzip or tar -zcvf firstName_secondName_assign_number.tar.gz firstName_secondName_assign_number

-----------------------------------------------------------------------

"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense.”

[Date: 10/17/2017 ]

-----------------------------------------------------------------------

Provide justification for Data Structures used in this assignment in
term of Big O complexity (time and/or space)


-----------------------------------------------------------------------

Provide list of citations (urls, etc.) from where you have taken code
(if any).

